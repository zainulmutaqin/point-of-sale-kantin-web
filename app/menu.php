<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class menu extends Model
{
    protected $table = 'menus';

    public function outlet_menus(){
        return $this->hasMany('App\outlet_menu');
    }
}
