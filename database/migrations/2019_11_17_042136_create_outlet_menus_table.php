<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutletMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outlet_menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreign('outlet_id')
                    ->references('id')
                    ->on('outlets');
            $table->foreign('menu_id')
                    ->references('id')
                    ->on('menus');
            $table->string('deskripsi');
            $table->integer('stok');
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outlet_menus');
    }
}
